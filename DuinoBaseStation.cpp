// Do not remove the include below

#include "DuinoBaseStation.h"

#include "SystemToolkit.h"
#include "DccToolkit.h"

// Create a current sensor reading on A0 with VCC = 5.0 volts
//ACS712CurrentSensor currentSensor(A0, 5000);
Max471CurrentSensor currentSensor(A0, 5000);

// Create a signal generator using timer 1, pin 12 which is OUTPUT COMPARE B
// for timer 1 on MEGA for example
SignalGenerator generator(1, 12, true);

// Create a track driver using previous generator & current sensor, also using
// pin 11 to switch track power ON/OFF
TrackDriver driver(11, &generator, &currentSensor);

// Create a command stream using USB serial port
SerialPortCommandStream usbStream;

// Create a command parser using the stream and our single track driver
CommandParser usbParser(&usbStream, &driver, NULL);

#if defined(DCC_ETHERNET_WIZNET_5100_ENABLED) || defined(DCC_ETHERNET_WIZNET_5200_ENABLED) || defined(DCC_ETHERNET_WIZNET_5500_ENABLED)
// Create a command stream using ETHERNET port
SocketCommandStream ipStream;

// Create a command parser using the stream and our single track driver
CommandParser ipParser(&ipStream, &driver, NULL);
#endif

void setup()
{
	driver.start();
	usbParser.start();
#if defined(DCC_ETHERNET_WIZNET_5100_ENABLED) || defined(DCC_ETHERNET_WIZNET_5200_ENABLED) || defined(DCC_ETHERNET_WIZNET_5500_ENABLED)
	ipParser.start();
#endif

	//driver.powerOn();
}

void readCV(byte cv)
{
	byte value = 0;
	bool result = driver.readCV(cv, &value);
	Serial.print("CV ");
	Serial.print(cv);
	Serial.print(" = ");
	Serial.print(value);

	if (result)
		Serial.println(" : OK");
	else
		Serial.println(" : Failed to read !");
}

int x = 1;
bool up = true;

void loop()
{

	 usbParser.run();
	 #if defined(DCC_ETHERNET_WIZNET_5100_ENABLED) || defined(DCC_ETHERNET_WIZNET_5200_ENABLED) || defined(DCC_ETHERNET_WIZNET_5500_ENABLED)
	 ipParser.run();
	 #endif


	//delay(2000);
		/*
	if (up)
		readCV(x++);
	else
		readCV(x--);

	if (x <= 1)
		up = true;
	if (x >= 140)
		up = false;
		*/
}
